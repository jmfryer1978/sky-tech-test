variable "region" {
  type = string 
  default = "eu-west-1"
}

locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

source "amazon-ebs" "example" {
  ami_name      = "JFRYER-91d575e6-ed9f-4dce-bff2-83913c84ca1b-${local.timestamp}"
  instance_type = "t2.micro"
  region        = "${var.region}"
  ami_groups    = ["all"]
  source_ami_filter {
    filters = {
      name                = "amzn2-ami-hvm*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["amazon"]
  }
  ssh_username = "ec2-user"
}

build {
  sources = ["source.amazon-ebs.example"]

  provisioner "file" {
    destination = "/home/ec2-user/nginx.conf"
    source      = "./nginx.conf"
  }

  provisioner "file" {
    destination = "/home/ec2-user/php-fpm.conf"
    source      = "./php-fpm.conf"
  }

  provisioner "file" {
    destination = "/home/ec2-user/index.php"
    source      = "./index.php"
  }

  provisioner "file" {
    destination = "/home/ec2-user/load-test.php"
    source      = "./load-test.php"
  }

  provisioner "file" {
    destination = "/home/ec2-user/load-test.sh"
    source      = "./load-test.sh"
  }

  provisioner "shell" {
    inline = [
      "sleep 30",
      "sudo mkdir -p /etc/nginx",
      "sudo mkdir -p /var/www/html",
      "sudo amazon-linux-extras install -y nginx1.12",
      "sudo yum install -y php-fpm",
      "sudo cp /home/ec2-user/nginx.conf /etc/nginx/nginx.conf",
      "sudo cp /home/ec2-user/php-fpm.conf /etc/php-fpm.d/www.conf",
      "sudo cp /home/ec2-user/index.php /usr/share/nginx/html/index.php",
      "sudo cp /home/ec2-user/load-test.php /usr/share/nginx/html/load-test.php",
      "sudo cp /home/ec2-user/load-test.sh /usr/share/nginx/html/load-test.sh",
      "sudo chown -R nginx:nginx /usr/share/nginx/",
      "sudo chmod 755 /usr/share/nginx/html/load-test.sh",
      "sudo chkconfig nginx on",
      "sudo chkconfig php-fpm on",
      "sudo systemctl start php-fpm",
      "sudo systemctl start nginx"
    ]
  }
}
