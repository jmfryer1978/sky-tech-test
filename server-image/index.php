<form action="/load-test.php">
    <input type="submit" value="Execute Load Test - Will increase host CPU to 100% for 2 minutes">
    <table border="0" cellspacing="1">
    <tr>
    <td>Host : <?php echo php_uname('n');?></td>
    </tr>
    <tr>
    <td> Once the above button has been pressed, you should see a new EC2 spun up in ASG after approx 60 seconds</td>
    </tr>
    <tr>
    <td> The load test runs for 2 minutes, after which point you should see the ASG scale back down</td>
    </tr>
    </table>       
</form>
