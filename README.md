# tech-test
## Gitlab CI Requirements
In order to use the pipeline to build and deploy the packer image, you will need to add environment variables for :
```
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_DEFAULT_REGION

```


## Usage
1. The CI pipeline can be used to build the packer image for the ami to use. Alternatively, you can clone the repo
   and execute `packer build techtest.pkr.hcl` from the `server-image` directory
   If you want to use the pipeline, you will need to add the relevant CI variables
   An image has already been created and is public, you may create another, the ASG always uses the most recent image 

2. The `web-cluster` directory contains the Terraform code for building the auto-scaling group based off this
   ami image (a lookup is performed to find it).

3. Once the ASG is created, and the web-server is running and can be accessed via the browser. At this point you
   can click the button on the web page to apply some CPU load to the node. This will prompt a new node to
   be added to the cluster. 

4. After 2 minutes, the load test will stop and the load will reduce, and you should see the number of 
   instances reduce back down to 3

5. As the hostname is displayed on the web page, this can be used to observe the nodes changing, so we can 
   see the load balancer is using a round-robin approach

6. Opted not to create a route53 address due to possible complexities/conflicts with hosted zones in your 
   test AWS account

## Terraform Project Requirements

### PLEASE NOTE, YOU WILL NEED A VALID AWS ENVIRONMENT TO EXECUTE THIS.

You should have the following set up either in ~/.aws/credentials, or as environment variables :

AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_DEFAULT_REGION

Also note, the `vpc_id` variable should be using your current VPC (or you may set a default)

The below has been created with the "terraform-docs" utility

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 3.27.0 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 3.27.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| aws\_account\_id | The aws account ID that created the AMI | `string` | `"953708868523"` | no |
| cpu\_max\_threshold | The maxumum threshold for the CPU, at which we scale up | `string` | `30` | no |
| cpu\_min\_threshold | The minumum threshold for the CPU, at which we scale down | `string` | `5` | no |
| elb\_port | The Port Number the ELB will be listening on | `number` | `80` | no |
| instance\_type | The instance type to use | `string` | `"t2.micro"` | no |
| region | The AWS Region to build in | `string` | `"eu-west-1"` | no |
| server\_port | The port the web server will be listening | `number` | `80` | no |
| service | Name of the service | `string` | `"tech-test"` | no |
| vpc\_id | The VPC ID | `string` | `"vpc-66bb3b00"` | no |

## Outputs

| Name | Description |
|------|-------------|
| lb\_dns\_name | DNS Name of the load balancer |

